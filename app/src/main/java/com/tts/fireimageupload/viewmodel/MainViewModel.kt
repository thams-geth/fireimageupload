package com.tts.fireimageupload.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.tts.fireimageupload.data.firebase.FirebaseImageService
import com.tts.fireimageupload.data.model.ImageData
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.asFlow
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.flow.flatMapConcat
import kotlinx.coroutines.flow.toList
import kotlinx.coroutines.launch


class MainViewModel : ViewModel() {

    private val _imageListData = MutableLiveData<List<ImageData>>()
    val imageListData: LiveData<List<ImageData>> = _imageListData

    init {
        viewModelScope.launch {
            FirebaseImageService.getImagesList().collect {
                _imageListData.postValue(it)
            }
        }
    }

    suspend fun <T> Flow<List<T>>.flattenToList() = flatMapConcat { it.asFlow() }.toList()
}