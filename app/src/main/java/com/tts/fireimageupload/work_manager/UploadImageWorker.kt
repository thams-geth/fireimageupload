package com.tts.fireimageupload.work_manager

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.NotificationManager.IMPORTANCE_HIGH
import android.app.PendingIntent
import android.content.Context
import android.content.Context.NOTIFICATION_SERVICE
import android.content.Intent
import android.graphics.Bitmap
import android.graphics.BitmapFactory
import android.os.Build.VERSION.SDK_INT
import android.os.Build.VERSION_CODES.O
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationCompat.PRIORITY_MAX
import androidx.work.Worker
import androidx.work.WorkerParameters
import com.google.firebase.firestore.ktx.firestore
import com.google.firebase.ktx.Firebase
import com.google.firebase.storage.ktx.storage
import com.tts.fireimageupload.R
import com.tts.fireimageupload.data.model.ImageData
import com.tts.fireimageupload.view.login.LoginActivity
import java.io.ByteArrayOutputStream
import java.util.Random

class UploadImageWorker(private val context: Context, workerParameters: WorkerParameters) :
    Worker(context, workerParameters) {

    companion object {
        private const val NOTIFICATION_NAME = "FireImageUpload"
        private const val NOTIFICATION_CHANNEL = "ImageUpload"
    }

    private val storageRef = Firebase.storage.reference
    private val db = Firebase.firestore


    override fun doWork(): Result {
        return try {
            val imagePath = inputData.getString("imagePath")
            val latitude = inputData.getDouble("latitude", 0.0)
            val longitude = inputData.getDouble("longitude", 0.0)

            // /storage/emulated/0/DCIM/Camera/IMG_20201104_211417.jpg
            // /storage/emulated/0/Android/data/com.tts.fireimageupload/files/Pictures/JPEG_20211222_235312_7043791404049843990.jpg
            val bitmap = BitmapFactory.decodeFile(imagePath)
            val byteArray = ByteArrayOutputStream()
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArray)
            val data = byteArray.toByteArray()

            val imageRef = storageRef.child("IMG_${System.currentTimeMillis()}.jpg")
            imageRef.putBytes(data).continueWithTask { task ->
                if (!task.isSuccessful) {
                    task.exception?.let {
                        throw it
                    }
                    showNotification("Oops! Something went wrong.Image didn't upload.")
                }
                imageRef.downloadUrl
            }.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    val downloadUri = task.result
                    val imageData = ImageData(
                        imageDownloadUri = downloadUri.toString(),
                        latitude = latitude,
                        longitude = longitude
                    )
                    db.collection("images").add(imageData).addOnSuccessListener {
                        showNotification("Image uploaded successfully.")
                    }.addOnFailureListener {
                        showNotification("Oops! Something went wrong.Image didn't upload.")
                    }
                } else {
                    showNotification("Oops! Something went wrong.Image didn't upload.")
                }
            }
            Result.success()
        } catch (e: Exception) {
            Result.failure()
        }
    }

    private fun showNotification(message: String) {
        val intent = Intent(context, LoginActivity::class.java).apply {
            flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK
        }
        val notificationManager = applicationContext.getSystemService(NOTIFICATION_SERVICE) as NotificationManager
        val pendingIntent = PendingIntent.getActivity(context, 0, intent, 0)
        val notification = NotificationCompat.Builder(context, NOTIFICATION_CHANNEL)
            .setSmallIcon(R.mipmap.ic_launcher)
            .setContentTitle("Image Upload")
            .setContentText(message)
            .setPriority(PRIORITY_MAX)
            .setContentIntent(pendingIntent)
            .setAutoCancel(true)

        if (SDK_INT >= O) {
            val channel = NotificationChannel(NOTIFICATION_CHANNEL, NOTIFICATION_NAME, IMPORTANCE_HIGH)
            notificationManager.createNotificationChannel(channel)
        }
        notificationManager.notify(Random().nextInt(), notification.build())
    }
}
