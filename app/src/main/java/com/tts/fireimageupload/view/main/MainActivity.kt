package com.tts.fireimageupload.view.main

import android.Manifest
import android.app.Activity
import android.content.Intent
import android.content.IntentSender
import android.content.pm.PackageManager
import android.location.Location
import android.net.Uri
import android.os.Bundle
import android.os.Environment
import android.os.Looper
import android.provider.MediaStore
import android.util.Log
import androidx.activity.result.contract.ActivityResultContracts
import androidx.appcompat.app.AlertDialog
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.core.content.FileProvider
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.WorkManager
import androidx.work.WorkRequest
import androidx.work.workDataOf
import com.google.android.gms.common.api.ResolvableApiException
import com.google.android.gms.location.FusedLocationProviderClient
import com.google.android.gms.location.LocationCallback
import com.google.android.gms.location.LocationRequest
import com.google.android.gms.location.LocationResult
import com.google.android.gms.location.LocationServices
import com.google.android.gms.location.LocationSettingsRequest
import com.google.android.gms.location.LocationSettingsResponse
import com.google.android.gms.location.SettingsClient
import com.google.android.gms.tasks.Task
import com.tts.fireimageupload.R
import com.tts.fireimageupload.data.model.ImageData
import com.tts.fireimageupload.databinding.ActivityMainBinding
import com.tts.fireimageupload.utils.PathFinder
import com.tts.fireimageupload.utils.toast
import com.tts.fireimageupload.view.map.MapsActivity
import com.tts.fireimageupload.viewmodel.MainViewModel
import com.tts.fireimageupload.work_manager.UploadImageWorker
import java.io.File
import java.io.IOException
import java.text.SimpleDateFormat
import java.util.Date
import java.util.Locale


class MainActivity : AppCompatActivity() {
    //    329360792661-d0c91ra00uhdqo2dneif7c6d774rg7tq.apps.googleusercontent.com
    lateinit var binding: ActivityMainBinding
    private lateinit var fusedLocationClient: FusedLocationProviderClient
    private lateinit var locationRequest: LocationRequest
    private var selectedOption: Int? = null
    private var latitude = 0.0
    private var longitude = 0.0
    lateinit var imageAdapter: ImageListAdapter
    lateinit var currentPhotoPath: String

    lateinit var mainViewModel: MainViewModel
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)

        mainViewModel = ViewModelProvider(this)[MainViewModel::class.java]
        setListeners()
        setAdapter()
        observeLiveData()
        observeLiveLocation()
    }

    private fun setListeners() {
        binding.fabAddImage.setOnClickListener {
            showImagePickOption()
        }
    }

    private fun setAdapter() {
        imageAdapter = ImageListAdapter(imageClick)
        binding.rvImage.adapter = imageAdapter
    }

    private fun observeLiveLocation() {
        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this)
        locationRequest = LocationRequest.create().apply {
            interval = 10000
            fastestInterval = 5000
            priority = LocationRequest.PRIORITY_HIGH_ACCURACY
        }


        val permission = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
        if (permission == PackageManager.PERMISSION_GRANTED) {
            fusedLocationClient.requestLocationUpdates(locationRequest, object : LocationCallback() {
                override fun onLocationResult(locationResult: LocationResult) {
                    val location: Location = locationResult.lastLocation
                    longitude = location.longitude
                    latitude = location.latitude
                    toast(this@MainActivity, "FusedLocation long : " + location.longitude + " lat :" + location.latitude)
                }
            }, Looper.getMainLooper())
        }

    }

    private fun observeLiveData() {
        mainViewModel.imageListData.observe(this, Observer {
            imageAdapter.submitList(it)

        })
    }

    private val imageClick = { data: ImageData ->
        Intent(this, MapsActivity::class.java).apply {
            putExtra("latitude", data.latitude)
            putExtra("longitude", data.longitude)
            startActivity(this)
        }
        toast(this, data.imageDownloadUri.toString())
    }

    private fun createLocationRequest() {
        val builder = LocationSettingsRequest.Builder().addLocationRequest(locationRequest)
        val client: SettingsClient = LocationServices.getSettingsClient(this)
        val task: Task<LocationSettingsResponse> = client.checkLocationSettings(builder.build())
        task.addOnSuccessListener { locationSettingsResponse ->
            when (selectedOption) {
                0 -> openCamera()
                1 -> openGallery()
                2 -> openGooglePhotos()
            }
            toast(this, "task success ${locationSettingsResponse.locationSettingsStates}")
        }

        task.addOnFailureListener { exception ->
            if (exception is ResolvableApiException) {
                // Location settings are not satisfied, but this can be fixed
                // by showing the user a dialog.
                try {
                    toast(this, "Please turn on Location")
                    // Show the dialog by calling startResolutionForResult(),
                    // and check the result in onActivityResult().
                    exception.startResolutionForResult(this@MainActivity, LOCATION_REQUEST)
                } catch (sendEx: IntentSender.SendIntentException) {
                    toast(this, "Location IntentSender.SendIntentException")
                    // Ignore the error.
                }
            }
        }
    }


    private fun hasPermissions(): Boolean {
        for (permission in permissions) {
            if (ContextCompat.checkSelfPermission(this, permission) != PackageManager.PERMISSION_GRANTED) {
                return false
            }
        }
        return true
    }

    private val requestMultiplePermissions = registerForActivityResult(ActivityResultContracts.RequestMultiplePermissions()) { permissions ->
        var allPermissionAdded = true
        permissions.entries.forEach {
            println("Permission key and value ${it.key} = ${it.value}")
            if (!it.value) allPermissionAdded = false
        }
        if (allPermissionAdded) {
            createLocationRequest()
        }
    }

    private fun requestPermissions() {
        requestMultiplePermissions.launch(permissions)
    }

    private fun openGooglePhotos() {
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).apply {
            setPackage("com.google.android.apps.photos")
//            isAppAvailable("com.google.android.apps.photos")
            if (this.resolveActivity(this@MainActivity.packageManager) != null)
                galleryLauncher.launch(this)
            else
                toast(this@MainActivity, "You don't have google photos app. Please install and try")
        }
        // com.google.android.apps.photos
    }

    private fun isAppAvailable(appName: String): Boolean {
        val pm: PackageManager = this.packageManager
        return try {
            pm.getPackageInfo(appName, PackageManager.GET_ACTIVITIES)
            true
        } catch (e: PackageManager.NameNotFoundException) {
            false
        }
    }

    override fun onStart() {
        super.onStart()
        Log.d(TAG, "onStart")
    }

    override fun onResume() {
        super.onResume()
        Log.d(TAG, "OnResume")
    }

    private val galleryLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            val uri = it.data?.data as Uri
            /* selectedBitmap = if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.P) {
                 ImageDecoder.decodeBitmap(ImageDecoder.createSource(contentResolver, uri))
             } else {
                 MediaStore.Images.Media.getBitmap(contentResolver, uri)
             }*/
            currentPhotoPath = PathFinder.getPathFromURI(this, uri) ?: ""
            uploadImage()
        }
    }

    private fun openGallery() {
        Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI).also {
            galleryLauncher.launch(it)
        }
    }

    private val openCameraLauncher = registerForActivityResult(ActivityResultContracts.StartActivityForResult()) {
        if (it.resultCode == Activity.RESULT_OK) {
            uploadImage()
        }
    }

    private fun uploadImage() {
        val uploadImageBuilder: WorkRequest = OneTimeWorkRequestBuilder<UploadImageWorker>()
            .setInputData(workDataOf("imagePath" to currentPhotoPath, "latitude" to latitude, "longitude" to longitude))
            .build()
        toast(this@MainActivity, "Your upload is in progress.We will notify once the image uploaded.")
        WorkManager.getInstance(this).enqueue(uploadImageBuilder)
    }


    @Throws(IOException::class)
    private fun createImageFile(): File {
        val timeStamp: String = SimpleDateFormat("yyyyMMdd_HHmmss", Locale.ENGLISH).format(Date())
        val storageDir: File? = getExternalFilesDir(Environment.DIRECTORY_PICTURES)
        return File.createTempFile(
            "JPEG_${timeStamp}_",
            ".jpg",
            storageDir
        ).apply {
            currentPhotoPath = absolutePath
        }
    }

    private fun openCamera() {
        Intent(MediaStore.ACTION_IMAGE_CAPTURE).also { takePictureIntent ->
            takePictureIntent.resolveActivity(packageManager)?.also {
                val photoFile: File? = try {
                    createImageFile()
                } catch (ex: IOException) {
                    null
                }
                photoFile?.also {
                    val photoURI: Uri = FileProvider.getUriForFile(
                        this,
                        "com.tts.fireimageupload.fileprovider",
                        it
                    )
                    takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, photoURI)
                    openCameraLauncher.launch(takePictureIntent)
                }
            }
        }
//        checkCameraPermissions()
    }

    private fun showImagePickOption() {
        val alert = AlertDialog.Builder(this)
            .setTitle("Select any")
            .setItems(R.array.option) { dialogInterface, i ->
                selectedOption = i
                checkAllPermissions()
                dialogInterface.dismiss()
                /* when (i) {
                     0 -> openCamera()
                     1 -> openGallery()
                     2 -> openGooglePhotos()
                     else -> dialogInterface.dismiss()
                 }*/
            }
        alert.create().show()
    }

    private fun checkAllPermissions() {
        if (hasPermissions()) {
            createLocationRequest()
        } else {
            requestPermissions()
        }
    }

    private fun checkCameraPermissions(): Boolean {
        return ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED
    }


    companion object {
        val permissions = arrayOf(
            Manifest.permission.CAMERA,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.ACCESS_COARSE_LOCATION,
        )
        const val LOCATION_REQUEST = 77
        const val TAG = "MainActivity"
    }

}