package com.tts.fireimageupload.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import com.tts.fireimageupload.data.model.ImageData
import com.tts.fireimageupload.databinding.ItemImageBinding

class ImageListAdapter(private val onNoteClick: (ImageData) -> Unit) :
    ListAdapter<ImageData, ImageViewHolder>(NotesDiffUtils) {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        return ImageViewHolder(
            ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        )
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.setNote(getItem(position), onNoteClick)
    }

    object NotesDiffUtils : DiffUtil.ItemCallback<ImageData>() {
        override fun areItemsTheSame(oldItem: ImageData, newItem: ImageData): Boolean {
            return oldItem.imageDownloadUri == newItem.imageDownloadUri
        }

        override fun areContentsTheSame(oldItem: ImageData, newItem: ImageData): Boolean {
            return oldItem == newItem
        }

    }
}

