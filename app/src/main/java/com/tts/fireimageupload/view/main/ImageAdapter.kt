package com.tts.fireimageupload.view.main

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.tts.fireimageupload.data.model.ImageData
import com.tts.fireimageupload.databinding.ItemImageBinding

class ImageAdapter(private val list: List<ImageData>, private val onNoteClick: (ImageData) -> Unit) : RecyclerView.Adapter<ImageViewHolder>() {

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImageViewHolder {
        val binding = ItemImageBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return ImageViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ImageViewHolder, position: Int) {
        holder.setNote(list[position], onNoteClick)
    }

    override fun getItemCount(): Int {
        return list.size
    }
}

class ImageViewHolder(private val view: ItemImageBinding) : RecyclerView.ViewHolder(view.root) {
    fun setNote(data: ImageData, onNoteClick: (ImageData) -> Unit) {
        Glide.with(view.imageView.context).load(data.imageDownloadUri).into(view.imageView)
        view.root.setOnClickListener {
            onNoteClick.invoke(data)
        }
    }
}