package com.tts.fireimageupload.data.firebase

import android.util.Log
import com.google.firebase.firestore.FirebaseFirestore
import com.tts.fireimageupload.data.model.ImageData
import com.tts.fireimageupload.view.main.MainActivity
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.channels.awaitClose
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.callbackFlow


object FirebaseImageService {

    @OptIn(ExperimentalCoroutinesApi::class)
    suspend fun getImagesList(): Flow<List<ImageData>> {
        val db = FirebaseFirestore.getInstance()
        return callbackFlow {
            val list = db.collection("images")
                .addSnapshotListener { value, e ->
                    if (e != null) {
                        Log.w(MainActivity.TAG, "Listen failed.", e)
                        return@addSnapshotListener
                    }
                    val map = value?.documents?.mapNotNull {
                        it.toObject(ImageData::class.java)
                    }
                    map?.let { this.trySend(it) }
                }
            awaitClose { list.remove() }
        }
    }

    /* fun getData() {
         val imageDataList = ArrayList<ImageData>()
         val db = FirebaseFirestore.getInstance()
         db.collection("images")
             .addSnapshotListener { value, e ->
                 if (e != null) {
                     return@addSnapshotListener
                 }
                 imageDataList.clear()
                 if (value != null && value.size() > 0) {
                     for (doc in value) {
                         imageDataList.add(doc.toObject(ImageData::class.java))
                     }
                 }
             }
     }*/
}