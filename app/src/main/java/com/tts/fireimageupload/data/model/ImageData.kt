package com.tts.fireimageupload.data.model

data class ImageData(
    val imageDownloadUri: String? = null,
    val latitude: Double? = null,
    val longitude: Double? = null
)